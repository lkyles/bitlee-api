#### bitlee App

This is the backend component part of a coding challenge application that provides URL shortening.

This application was built with ExpressJS 

#### Running the application

visit the deployed version at https://bitlee.org (https://api.bitlee.org)

You can run locally with
```
npm install
npm start
```

#### Testing the application

To test the app run 

```
npm test
```

#### Deployment

The frontend app is deployed on AWS Lambda, deployed via the bitbucket pipeline

It uses ClaudiaJS to automate the deployment process
