const {getOriginalUrl, postNewUrl} = require('../controllers/urlController')

describe('Url Controller', () => {
    let params = {originalUrl: 'http://test.com/home', domain: 'https://bitlee.org'}
    const urlModel = { 
      create: jest.fn(),
      get: jest.fn(),
      query: jest.fn(() => ({usingIndex: jest.fn(() => ({exec: jest.fn()}))})) 
    }
    const res = {json: jest.fn(), status: jest.fn(() => ({send: jest.fn()}))}

    afterEach(() => {
      jest.clearAllMocks()
    });

    test('should call dynamoDB with new url properties', () => {
      urlModel.get = jest.fn().mockImplementation((_, cb) => cb(null, null))
      const postUrl = postNewUrl(urlModel)
      const req = {body: params}

      postUrl(req, res)

      expect(urlModel.create.mock.calls[0][0].originalUrl).toEqual(params.originalUrl)
      expect(urlModel.create.mock.calls[0][0].urlCode).toBeDefined()
      expect(urlModel.create.mock.calls[0][0].shortUrl).toBeDefined()
    })

    test('should call dynamoDB to get url entry by urlCode', () => {
      params = {urlCode: '1234'}
      const getUrl = getOriginalUrl(urlModel)
      const req = {params}

      getUrl(req, res)

      expect(urlModel.query.mock.calls[0][0]).toContain(params.urlCode)
    })

    test('should not create url entry if original url is not valid', () => {
      params = {originalUrl: 'blah', domain: 'https://bitlee.org'}
      const postUrl = postNewUrl(urlModel)
      const req = {body: params}

      postUrl(req, res)

      expect(urlModel.create.mock.calls.length).toEqual(0)
      expect(res.status.mock.calls[0][0]).toEqual(422)
    })
})
