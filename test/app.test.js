const request = require('supertest')
const app = require('../app')

const urlModel = require('../models/url')

describe.only('App url routes', () => {

  beforeEach(() => {
    jest.mock('../models/url')
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  test('should successfully make a POST method', async (done) => {
    let urlRes = {
      createdAt: "2018-08-31T11:06:25.441Z",
      originalUrl: "http://test.com/about",
      shortUrl: "https://bitlee.org/zVwWeokuZ",
      urlCode: "zVwWeokuZ"
    }
    urlModel.get = jest.fn().mockImplementation((_, cb) => cb(null, null))
    urlModel.create = jest.fn().mockImplementation((_, cb) => cb(null, urlRes))

    const response = await request(app)
    .post('/url')
    .send({domain: 'https://bitlee.org/', originalUrl: 'https://test.com/htasd'})
    expect(response.statusCode).toBe(200)
    expect(response.body.toString()).toBe(urlRes.toString())

    done()
  })

  test('should handle 422 when request body does not have a valid url when making a POST method', async (done) => {
    urlModel.get = jest.fn().mockImplementation((_, cb) => cb(null, null))
    urlModel.create = jest.fn().mockImplementation((_, cb) => cb(null, urlRes))

    const response = await request(app)
    .post('/url')
    .send({domain: 'https://bitlee.org/', originalUrl: 'blahj'})
    expect(response.statusCode).toBe(422)

    done()
  })

  test('should successfully make a GET method and result in a redirect', async (done) => {
    let urlRes = {
      Items: [
        { attrs: {
            createdAt: "2018-08-31T11:06:25.441Z",
            originalUrl: "http://test.com/about",
            shortUrl: "https://bitlee.org/zVwWeokuZ",
            urlCode: "zVwWeokuZ"
          }
        }
      ]
    }
    urlModel.query = () => ({usingIndex: () => ({exec: jest.fn().mockImplementation(cb => cb(null, urlRes))})})

    const response = await request(app)
    .get('/url/zVwWeokuZ')
    expect(response.statusCode).toBe(200)

    done()
  })

  test('should handle 422 when no url is dfound with the given urlCode when making a GET method', async (done) => {
    let urlRes = {
      Items: [
      ]
    }
    urlModel.query = () => ({usingIndex: () => ({exec: jest.fn().mockImplementation(cb => cb(null, urlRes))})})

    const response = await request(app)
    .get('/url/zVwWeokuZ')
    expect(response.statusCode).toBe(422)

    done()
  })
})
