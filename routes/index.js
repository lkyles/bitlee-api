const express = require('express')
const router = express.Router()
const Url = require('../models/url')
const { getOriginalUrl, postNewUrl } = require('../controllers/urlController')

router.get('/url/:urlCode', getOriginalUrl(Url))
router.post('/url', postNewUrl(Url))

module.exports = router
