const isUrl = require('is-url')
const shortid = require('shortid')

const getOriginalUrl = (Url) => (req, res, next) => {
  Url.query(req.params.urlCode)
  .usingIndex('urlCodeIndex')
  .exec((err, url) => {
    if (err) {return next(err)}
    if (url.Items.length > 0) {
      res.json({url: url.Items[0].attrs.originalUrl})
    } else {
      res.status(422).send({message: 'No url found'})
    }
  })
}

const postNewUrl = (Url) => (req, res, next) => {
  const {originalUrl, domain} = req.body
  if (isUrl(originalUrl)) {
    Url.get(originalUrl, function (err, url) {
      if (err) {return next(err)}
      if (url) {
        return res.json(url)
      } else {
        const urlCode = shortid.generate()
        Url.create({urlCode, originalUrl, shortUrl: `${domain}${urlCode}`}, (err, newUrl) => {
          if (err) {return next(err)}
          res.json(newUrl)
        })
      }
    })
  } else {
    res.status(422).send({message: 'Not a valid url'})
  }
}

module.exports = {
  getOriginalUrl,
  postNewUrl
}
