const dynamo = require('dynamodb')
const Joi = require('joi')

const Url = dynamo.define('Url', {
  hashKey : 'originalUrl',
  timestamps : true,
  schema : {
    originalUrl : Joi.string(),
    shortUrl    : Joi.string(),
    urlCode     : Joi.string()
  },
  indexes : [{
    hashKey : 'urlCode', name : 'urlCodeIndex', type : 'global'
  }]
});

module.exports = Url
