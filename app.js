const createError = require('http-errors')
const express = require('express')
const path = require('path')
const logger = require('morgan')
const bodyParser = require('body-parser')
const dynamo = require('dynamodb')
const app = express()
const cors = require('cors')

require('dotenv').config()

dynamo.AWS.config.update({
    region: process.env.REGION,
    endpoint: process.env.DB_ENDPOINT
})

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded())

const corsOptions = function (req, callback) {  
    callback(null, { origin: process.env.CORS_ORIGIN, optionsSuccessStatus: 200 })
}
app.use(cors(corsOptions))

// need to have both / and /v1 due to api gateway proxying for custom domains & API gateway URL
const routes = require('./routes/index')
app.use('/v1', routes)
app.use('/', routes)

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    next(createError(404))
})

/// error handlers

// development error handler
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500)
        res.render('error', {
            message: err.message,
            error: err
        })
    })
}

// production error handler

app.use(function(err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
        message: err.message,
        error: {}
    })
})


module.exports = app
